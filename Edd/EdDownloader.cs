﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Configuration;

namespace Edd
{
    public class EdDownloader
    {
        public EdDownloader()
        {
            MaxThreads = int.Parse(ConfigurationSettings.AppSettings["MaxThreads"].ToString());
            DownloadLocation = ConfigurationSettings.AppSettings["DownloadLocation"].ToString();
            Files = new List<ManifestFile>();
            LoadFiles("manifest.xml");
            CreateFolders();
            TotalFiles = Files.Count();
            TotalBytes = Files.Sum(f => (decimal)f.Size);
            CurrentDownloads = new List<ManifestFile>();            
        }

        public string DownloadLocation { get; set; }
        public List<ManifestFile> Files;
        public int MaxThreads { get; set; }
        public int CurrentThreads { get; set; }
        public int TotalFiles { get; set; }
        public decimal TotalBytes { get; set; }
        public int FilesStarted { get; set; }
        public List<ManifestFile> CurrentDownloads;

        object threadCountLock = new object();
        object fileLock = new object();
        object downloadedBytesLock = new object();

        private void LoadFiles(string xmlPath)
        {
            XDocument doc = XDocument.Load(xmlPath);
            this.Files = doc.Descendants("File")
                .Select(t =>
                    new ManifestFile
                    {
                        Path = t.Element("Path").Value,
                        Download = t.Element("Download").Value,
                        Size = int.Parse(t.Element("Size").Value),
                        Hash = t.Element("Hash").Value
                    }
                )
                .ToList();
        }

        private void CreateFolders()
        {
            foreach (var file in Files)
            {
                var folder = Path.GetDirectoryName(DownloadLocation + file.Path);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
            }
        }

        public void StartDownloads()
        {
            while (FilesStarted < TotalFiles)
            {
                lock (threadCountLock)
                {
                    if (CurrentThreads < MaxThreads)
                    {
                        CurrentThreads++;
                        var index = FilesStarted;                     
                        FilesStarted++;                        
                        ThreadStart work = delegate
                        {
                            Download(index);
                        };
                        new Thread(work).Start();
                    }
                }
            }
        }

        private async void Download(int index)
        {
            using (var client = new MyWebClient())
            {
                client.Index = index;
                var file = Files[index];
                var savePath = DownloadLocation + file.Path;
                var saveFolder = Path.GetDirectoryName(savePath);
                client.DownloadProgressChanged += client_DownloadProgressChanged;
                file.IsDownloading = true;
                await client.DownloadFileTaskAsync(file.Download, savePath);
                file.IsDownloading = false;
                lock (threadCountLock)
                {
                    CurrentThreads--;
                }                
            }
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            var client = sender as MyWebClient;
            Files[client.Index].Percentage = (decimal)e.BytesReceived / (decimal)e.TotalBytesToReceive;
        }


        protected virtual void OnDownloadStarted(DownloadEventArgs e)
        {
            EventHandler<DownloadEventArgs> handler = DownloadStarted;
            if (handler != null) handler(this, e);
        }
        public event EventHandler<DownloadEventArgs> DownloadStarted;

        protected virtual void OnDownloadComplete(DownloadEventArgs e)
        {
            EventHandler<DownloadEventArgs> handler = DownloadComplete;
            if (handler != null) handler(this, e);
        }
        public event EventHandler<DownloadEventArgs> DownloadComplete;

        protected virtual void OnProgressChanged(EventArgs e)
        {
            EventHandler<EventArgs> handler = ProgressChanged;
            if (handler != null) handler(this, e);
        }
        public event EventHandler<EventArgs> ProgressChanged;

    }
}
