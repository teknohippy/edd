﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Edd
{
    public class ManifestFile
    {
        public string Path { get; set; }
        public string Hash { get; set; }
        public long Size { get; set; }
        public string Download { get; set; }
        public decimal Percentage { get; set; }
        public bool IsDownloading { get; set; }
    }
}
