﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Edd
{
    class Program
    {
        static EdDownloader d;
        static object printLock = new object();
        static Timer timer = new Timer();
        static List<ManifestFile> downloadingFiles;

        static void Main(string[] args)
        {
            d = new EdDownloader();
            timer.Interval = 100;
            timer.Elapsed += timer_Elapsed;
            timer.Start();
            d.StartDownloads();
            Console.WriteLine("Done");
            Console.ReadLine();
        }

        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PrintProgress();
        }



        static void PrintProgress()
        {
            downloadingFiles = new List<ManifestFile>(d.Files.Where(f => f.IsDownloading));
            var bytesDownloaded = (long)(d.Files.Sum(f => f.Size * f.Percentage));
            Console.SetCursorPosition(0, 0);
            ClearCurrentConsoleLine();
            Console.WriteLine("{0:P2} {1} of {2} files. {3} of {4} bytes.",
                bytesDownloaded / d.TotalBytes,
                d.FilesStarted,
                d.TotalFiles,
                bytesDownloaded,
                d.TotalBytes);
            int totalStars = (int)(79 * (bytesDownloaded / d.TotalBytes));
            Console.WriteLine(new String('*', totalStars));

            foreach (var item in downloadingFiles)
            {
                ClearCurrentConsoleLine();
                Console.WriteLine(item.Path.Length < 79 ? item.Path : item.Path.Substring(0, 79));
                int noOfStars = (int)(79 * item.Percentage);
                ClearCurrentConsoleLine();
                Console.WriteLine(new String('*', noOfStars));
            }

        }

        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }



    }
}
